QMK keymap
==========

My personal keymap, ported over from the previous JSON-based variant, which kept
giving me grief when compiling.

Clone this into `<firmware>/keyboards/lily58/keymaps/doenietzomoeilijk` and 
compile with `make lily58:doenietzomoeilijk` (or setup QMK to always pick this
keyboard/mapping combo with `qmk config`).

Uses [Achordion](https://getreuer.info/posts/keyboards/achordion/index.html) and
the Luna animation from the `sofle/helltm` keymap.
