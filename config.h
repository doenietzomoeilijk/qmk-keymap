#pragma once

// See https://docs.qmk.fm/#/config_options?id=the-configh-file

// -------------------- MEMORY SAVING
// See https://docs.qmk.fm/#/squeezing_avr
// No need for mechanical lock key support.
#undef  LOCKING_RESYNC_ENABLE
#undef  LOCKING_SUPPORT_ENABLE
// No support for more than 8 layers (can be 16BIT for 16 layers).
#define DYNAMIC_KEYMAP_LAYER_COUNT 7
#define LAYER_STATE_8BIT

// -------------------- SPLIT SETUP
// See https://docs.qmk.fm/#/feature_split_keyboard?id=handedness-by-eeprom
#define EE_HANDS
#define SPLIT_ACTIVITY_ENABLE
#define SPLIT_LAYER_STATE_ENABLE
#define SPLIT_LED_STATE_ENABLE
#define SPLIT_MODS_ENABLE
/* #define SPLIT_OLED_ENABLE */
#define SPLIT_TRANSACTION_IDS_USER USER_SYNC_A, USER_SYNC_B
#define SPLIT_USB_DETECT
#define SPLIT_WPM_ENABLE

// -------------------- HARDWARE MISC
#define USB_MAX_POWER_CONSUMPTION 100
#define USB_SUSPEND_WAKEUP_DELAY 100
#define LED_CAPS_LOCK_PIN B0 // Alternatively D5
#define LED_PIN_ON_STATE 0

// -------------------- TAPS AND SHOTS
// See https://docs.qmk.fm/#/tap_hold?id=tap-or-hold-decision-modes
#define PERMISSIVE_HOLD
// #define HOLD_ON_OTHER_KEY_PRESS
#define TAPPING_TERM 150
#define TAPPING_TERM_PER_KEY

#define ONESHOT_TAP_TOGGLE 2 // How many taps before oneshot toggle is triggered
#define QUICK_TAP_TERM_PER_KEY // QUICK_TAP_TERM defaults to TAPPING_TERM

// -------------------- LEADER KEY
#ifdef LEADER_ENABLE
#define LEADER_TIMEOUT 250
#define LEADER_PER_KEY_TIMING
#endif

// -------------------- FLUFF
#define CAPS_WORD_INVERT_ON_SHIFT
#define CAPS_WORD_IDLE_TIMEOUT 5000
#define LAYER_LOCK_IDLE_TIMEOUT 10000

// -------------------- OLED DISPLAYS
#ifdef OLED_DRIVER_ENABLE
#define SPLIT_OLED_ENABLE

#define OLED_BRIGHTNESS 200
#define OLED_TIMEOUT 10000

#define OLED_FADE_OUT
/* #define OLED_FADE_OUT_INTERVAL 8 */
#endif
