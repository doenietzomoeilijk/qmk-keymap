#pragma once
#include QMK_KEYBOARD_H

// Home row mods (QWERTY)
#define GUI_A   LGUI_T(KC_A)
#define ALT_S   LALT_T(KC_S)
#define CTL_D   LCTL_T(KC_D)
#define SFT_F   LSFT_T(KC_F)
#define RALT_G  RALT_T(KC_G)
#define RALT_H  RALT_T(KC_H)
#define SFT_J   RSFT_T(KC_J)
#define CTL_K   RCTL_T(KC_K)
#define ALT_L   LALT_T(KC_L)
#define GUI_SC  LGUI_T(KC_SCLN)

// Home row mods (Colemak)
#define CALT_R  LALT_T(KC_R)
#define CCTL_S  LCTL_T(KC_S)
#define CSFT_T  LSFT_T(KC_T)
#define CRALT_M RALT_T(KC_M)
#define CSFT_N  RSFT_T(KC_N)
#define CCTL_E  RCTL_T(KC_E)
#define CALT_I  LALT_T(KC_I)
#define CGUI_O  LGUI_T(KC_O)

// Thumbs
#define FUN_TAB LT(_FUNCTION,KC_TAB)
#define NAV_ENT LT(_NAVIGATION,KC_ENT)
#define NAV_SPC LT(_NAVIGATION,KC_SPC)
#define NUM_SPC LT(_NUMBERS,KC_SPC)
#define SYM_ENT LT(_SYMBOLS,KC_ENT)
// Newer set of thumbs, some of these are double to allow for experimentation.
// See https://github.com/ghostbuster91/blogposts/blob/main/42-keys/main.md
#define FUN_DEL LT(_FUNCTION,KC_DEL)
#define NAV_BSP LT(_NAVIGATION,KC_BSPC)
#define NUM_DEL LT(_NUMBERS,KC_DEL)
#define NUM_TAB LT(_NUMBERS,KC_TAB)
#define SYM_SPC LT(_SYMBOLS,KC_SPC)

// Other buttons
#define _MO_ADJ MO(_ADJUST)
#define _DF_QWE DF(_QWERTY)
#define _DF_COL DF(_COLEMAK)

enum layer_number {
    _QWERTY,
    _COLEMAK,
    _NAVIGATION,
    _NUMBERS,
    _SYMBOLS,
    _FUNCTION,
    _ADJUST
};

enum custom_keycode {
    OBR_UP = SAFE_RANGE,
    OBR_DN,
    _L_LOCK 
};

