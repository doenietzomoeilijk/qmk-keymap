#ifdef OLED_ENABLE
#include "oled.h"
#include "keymap.h"
#include "lib/layer_lock.h"
#include "quantum.h"
#include <stdint.h>
#include <stdio.h>

const uint16_t notification_lifetime = 1000;
const uint8_t single_bit_masks[8] = {127, 191, 223, 239, 247, 251, 253, 254};
uint16_t notification_timeout = 0;
uint32_t oled_task_timer = 0;

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
    return OLED_ROTATION_270;
  /* if (!is_keyboard_master()) { */
  /*   return OLED_ROTATION_180; */
  /* } */

  /* return rotation; */
}

void notify(const char *data) {
    oled_clear();
    oled_write(data, false);
    notification_timeout = timer_read32() + notification_lifetime;
}

bool process_oled_brightness(uint16_t keycode, keyrecord_t* record) {
    if (!record->event.pressed || (keycode != OBR_UP && keycode != OBR_DN)) {
        return true;
    }

    uint8_t bri = oled_get_brightness();
    uprintf("Current OLED bri=%03u\n", bri);
    if (keycode == OBR_DN) {
        bri = bri - 10;
        if (bri < 10) {
            bri = 10;
        }
    } else if (keycode == OBR_UP) {
        bri = bri + 10;
        if (bri > 240) {
            bri = 240;
        }
    }

    uprintf("New OLED bri=%03u\n", bri);
    bri = oled_set_brightness(bri);

    oled_clear();
    oled_write("Bright:", false);
    oled_write(get_u8_str(bri, '0'), false);
    notification_timeout = timer_read32() + notification_lifetime;

    return false;
}

static void fade_display(void) {
    //Define the reader structure
    oled_buffer_reader_t reader;
    uint8_t buff_char;
    /* if (random() % 30 == 0) { */
        srand(timer_read());
        // Fetch a pointer for the buffer byte at index 0. The return structure
        // will have the pointer and the number of bytes remaining from this
        // index position if we want to perform a sequential read by
        // incrementing the buffer pointer
        reader = oled_read_raw(0);
        //Loop over the remaining buffer and erase pixels as we go
        for (uint16_t i = 0; i < reader.remaining_element_count; i++) {
            dprintf("looping over buffer, i=%d element=%d\n", i, reader.current_element);
            //Get the actual byte in the buffer by dereferencing the pointer
            buff_char = *reader.current_element;
            if (buff_char != 0) {
                dprint("buf char not 0\n");
                oled_write_raw_byte(buff_char & single_bit_masks[rand() % 8], i);
            }
            //increment the pointer to fetch a new byte during the next loop
            reader.current_element++;
        }
    /* } */
}

bool oled_task_user(void) {
    if (!is_keyboard_master()) {
        return true;
    }

    oled_task_timer = timer_read32();
    if (notification_timeout) {
        dprintf("notification timer %d, timer %d\n", notification_timeout, oled_task_timer);

        if (oled_task_timer < notification_timeout) {
            // We have a notification timer running, and it's not run out yet.
            dprint("We have a notification timer running\n");
            return false;
        }

        // We hade a timer but it's done. Reset it and clear the screen.
        dprint("We had a notification timer running, clearing\n");
        fade_display();
        notification_timeout = 0;
    }

    oled_clear();
    // Should be moved to layer change user handler
    /*
    uint8_t lr = get_highest_layer(layer_state);
    if (lr == _QWERTY || lr == _COLEMAK) {
        if (layer_state_cmp(default_layer_state, _QWERTY)) {
            oled_write_ln(" QWE", false);
        } else if (layer_state_cmp(default_layer_state, _COLEMAK)) {
            oled_write_ln(" COL", false);
        }
    }
    */

    if (is_layer_locked(_NAVIGATION)) {
        oled_write_ln(" NAV lock ", true);
    } else if (is_layer_locked(_NUMBERS)) {
        oled_write_ln(" NUM lock ", true);
    } else if (is_layer_locked(_FUNCTION)) {
        oled_write_ln(" FUN lock ", true);
    } else if (is_layer_locked(_SYMBOLS)) {
        oled_write_ln(" SYM lock ", true);
    } else if (is_layer_locked(_ADJUST)) {
        oled_write_ln(" ADJ lock ", true);
    /* } else { */
    /*     oled_write_ln("     ", false); */
    }

    led_t led_usb_state = host_keyboard_led_state();
    if (led_usb_state.caps_lock) {
        oled_write_ln("Caps lock", false);
    } else if (is_caps_word_on()) {
        oled_write_ln("Caps word", false);
    }

    /* oled_set_cursor(0, 1); */
    /* oled_write_ln(lr == _NAVIGATION ? (is_layer_locked(_NAVIGATION) ? "+NAV" : "+nav") : "", false); */
    /* oled_write_ln(lr == _NUMBERS    ? (is_layer_locked(_NUMBERS)    ? "+NUM" : "+num") : "", false); */
    /* oled_write_ln(lr == _FUNCTION   ? (is_layer_locked(_FUNCTION)   ? "+FUN" : "+fun") : "", false); */
    /* oled_write_ln(lr == _SYMBOLS    ? (is_layer_locked(_SYMBOLS)    ? "+SYM" : "+sym") : "", false); */

    return true;
}
#endif
