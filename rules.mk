AUDIO_ENABLE            = no
AVR_USE_MINIMAL_PRINTF  = yes
BACKLIGHT_ENABLE        = no
BLUETOOTH_ENABLE        = no
BOOTMAGIC_ENABLE        = no
CAPS_WORD_ENABLE        = yes
COMMAND_ENABLE          = no
CONSOLE_ENABLE          = yes
DEFERRED_EXEC_ENABLE    = yes
EXTRAFLAGS             += -flto
EXTRAKEY_ENABLE         = yes
GRAVE_ESC_ENABLE        = no
LEADER_ENABLE           = no
LTO_ENABLE              = yes
MAGIC_ENABLE            = no
MIDI_ENABLE             = no
MOUSEKEY_ENABLE         = yes
NKRO_ENABLE             = no
# OLED_DRIVER             = SSD1306
OLED_DRIVER_ENABLE      = no
RGBLIGHT_ENABLE         = no
SLEEP_LED_ENABLE        = no
SPACE_CADET_ENABLE      = no
SPLIT_KEYBOARD          = yes
TAP_DANCE_ENABLE        = no
VIA_ENABLE              = no
WPM_ENABLE              = no

SRC += doenietzomoeilijk.c \
	   lib/achordion.c \
	   lib/layer_lock.c
