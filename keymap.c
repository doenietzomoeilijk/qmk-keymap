#include QMK_KEYBOARD_H

#include "keymap.h"

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
[_QWERTY] = LAYOUT(
    KC_ESC,   KC_1,     KC_2,     KC_3,     KC_4,     KC_5,                         KC_6,     KC_7,     KC_8,     KC_9,     KC_0,     KC_MINS, \
    KC_TAB,   KC_Q,     KC_W,     KC_E,     KC_R,     KC_T,                         KC_Y,     KC_U,     KC_I,     KC_O,     KC_P,     KC_BSPC, \
    CW_TOGG,  GUI_A,    ALT_S,    CTL_D,    SFT_F,    RALT_G,                       RALT_H,   SFT_J,    CTL_K,    ALT_L,    GUI_SC,   KC_QUOT, \
    KC_GRV,   KC_Z,     KC_X,     KC_C,     KC_V,     KC_B,     _MO_ADJ,  XXXXXXX,  KC_N,     KC_M,     KC_COMM,  KC_DOT,   KC_SLSH,  KC_BSLS, \
                                  XXXXXXX,  FUN_TAB,  NAV_BSP,  NUM_DEL,  NAV_ENT,  SYM_SPC,  KC_ESC,   XXXXXXX
),

[_COLEMAK] = LAYOUT(
    KC_ESC,   KC_1,     KC_2,     KC_3,     KC_4,     KC_5,                         KC_6,     KC_7,     KC_8,     KC_9,     KC_0,     KC_MINS, \
    KC_TAB,   KC_Q,     KC_W,     KC_F,     KC_P,     KC_B,                         KC_J,     KC_L,     KC_U,     KC_Y,     KC_SCLN,  KC_BSPC, \
    CW_TOGG,  GUI_A,    CALT_R,   CCTL_S,   CSFT_T,   RALT_G,                       CRALT_M,  CSFT_N,   CCTL_E,   CALT_I,   CGUI_O,   KC_QUOT, \
    KC_GRV,   KC_Z,     KC_X,     KC_C,     KC_D,     KC_V,     _MO_ADJ,  XXXXXXX,  KC_K,     KC_H,     KC_COMM,  KC_DOT,   KC_SLSH,  KC_BSLS, \
                                  XXXXXXX,  FUN_TAB,  NAV_BSP,  NUM_DEL,  NAV_ENT,  SYM_SPC,  KC_ESC,   XXXXXXX
),

[_NAVIGATION] = LAYOUT(
    _______,  _______,  _______,  _______,  _______,  _______,                      _______,  _______,  _______,  _______,  _______,  _______, \
    XXXXXXX,  KC_MPLY,  KC_MUTE,  KC_VOLD,  KC_VOLU,  XXXXXXX,                      KC_WBAK,  KC_WH_D,  KC_WH_U,  KC_WFWD,  KC_CAPS,  XXXXXXX, \
    XXXXXXX,  KC_LGUI,  KC_LALT,  KC_LCTL,  KC_LSFT,  KC_RALT,                      KC_LEFT,  KC_DOWN,  KC_UP,    KC_RGHT,  CW_TOGG,  XXXXXXX, \
    XXXXXXX,  XXXXXXX,  XXXXXXX,  KC_BRID,  KC_BRIU,  XXXXXXX,  XXXXXXX,  XXXXXXX,  KC_HOME,  KC_PGDN,  KC_PGUP,  KC_END,   KC_PSCR,  _L_LOCK, \
                                  XXXXXXX,  KC_TAB,   KC_BSPC,  KC_DEL,   XXXXXXX,  KC_BSPC,  KC_APP,   QK_BOOT
),

[_NUMBERS] = LAYOUT(
    _______,  _______,  _______,  _______,  _______,  _______,                      _______,  _______,  _______,  _______,  _______,  _______, \
    XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,                      KC_LBRC,  KC_7,     KC_8,     KC_9,     KC_RBRC,  _______, \
    XXXXXXX,  KC_LGUI,  KC_LALT,  KC_LCTL,  KC_LSFT,  KC_RALT,                      KC_MINS,  KC_4,     KC_5,     KC_6,     KC_EQL,   XXXXXXX, \
    XXXXXXX,  XXXXXXX,  XXXXXXX,  KC_COMM,  KC_DOT,   XXXXXXX,  XXXXXXX,  XXXXXXX,  KC_LT,    KC_1,     KC_2,     KC_3,     KC_GT,    _L_LOCK, \
                                  XXXXXXX,  KC_TAB,   KC_BSPC,  KC_DEL,   KC_PENT,  KC_0,     KC_DOT,   XXXXXXX
),

[_SYMBOLS] = LAYOUT(
    _______,  _______,  _______,  _______,  _______,  _______,                      _______,  _______,  _______,  _______,  _______,  _______, \
    XXXXXXX,  KC_LT,    KC_MINS,  KC_EQL,   KC_GT,    XXXXXXX,                      XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX, \
    XXXXXXX,  KC_LBRC,  KC_RBRC,  KC_LPRN,  KC_RPRN,  KC_BSLS,                      KC_RALT,  KC_RSFT,  KC_RCTL,  KC_LALT,  KC_RGUI,  XXXXXXX, \
    _L_LOCK,  KC_LCBR,  KC_RCBR,  KC_GRV,   KC_QUOT,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  _L_LOCK, \
                                  XXXXXXX,  KC_TAB,   KC_BSPC,  KC_DEL,   XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX
),

[_FUNCTION] = LAYOUT(
    XXXXXXX,  KC_F1,    KC_F2,    KC_F3,    KC_F4,    KC_F5,                        XXXXXXX,  KC_F10,   KC_F11,   KC_F12,   XXXXXXX,  XXXXXXX, \
    XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,                      XXXXXXX,  KC_F7,    KC_F8,    KC_F9,    KC_F12,   OBR_UP,  \
    XXXXXXX,  KC_LGUI,  KC_LALT,  KC_LCTL,  KC_LSFT,  KC_RALT,                      XXXXXXX,  KC_F4,    KC_F5,    KC_F6,    KC_F11,   OBR_DN,  \
    XXXXXXX,  XXXXXXX,  XXXXXXX,  KC_CALC,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  KC_F1,    KC_F2,    KC_F3,    KC_F10,   _L_LOCK, \
                                  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX
),

[_ADJUST] = LAYOUT(
    XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,                      XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX, \
    XXXXXXX,  _DF_QWE,  XXXXXXX,  XXXXXXX,  QK_RBT,   XXXXXXX,                      XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX, \
    XXXXXXX,  XXXXXXX,  XXXXXXX,  DB_TOGG,  XXXXXXX,  XXXXXXX,                      XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX, \
    XXXXXXX,  XXXXXXX,  XXXXXXX,  _DF_COL,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  _L_LOCK, \
                                  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX
)
};
