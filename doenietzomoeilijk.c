#include "config.h"
#include "keymap.h"
#include "lib/achordion.h"
#include "lib/layer_lock.h"
#include "quantum.h"
#include <stdint.h>
#include <stdio.h>

// ------------------------------------------------------------ PROCESS

#ifdef OLED_DRIVER_ENABLE
const bool process_oled_brightness(uint16_t keycode, keyrecord_t* record);
#endif

bool process_record_user(uint16_t keycode, keyrecord_t* record) {
#ifdef CONSOLE_ENABLE
    dprintf(
            "KL kc=0x%04X col=%02u row=%02u press=%s time=%5u int=%u count=%u\n",
            keycode,
            record->event.key.col,
            record->event.key.row,
            record->event.pressed ? "pressed " : "released",
            record->event.time,
            record->tap.interrupted,
            record->tap.count);
#endif

#ifdef OLED_DRIVER_ENABLE
    if (!process_oled_brightness(keycode, record)) { return false; }
#endif

    if (!process_achordion(keycode, record)) { return false; }
    if (!process_layer_lock(keycode, record, _L_LOCK)) { return false; }

    return true;
}

// ------------------------------------------------------------ ACHORDION

bool achordion_chord(uint16_t tap_hold_keycode, keyrecord_t* tap_hold_record,
                     uint16_t other_keycode, keyrecord_t* other_record) {
    // Allow same-hand alt-spacing and ctrl-tabbing and so on.
    if (other_keycode == NAV_SPC 
            || other_keycode == NAV_ENT
            || other_keycode == FUN_TAB 
            || other_keycode == NAV_BSP 
            || other_keycode == NUM_DEL) {
        return true;
    }
    if (other_record->event.key.row >= 4) {
        return true;
    }

    // Otherwise, follow the opposite hands rule.
    return achordion_opposite_hands(tap_hold_record, other_record);
}

void matrix_scan_user(void) {
    achordion_task();
    layer_lock_task();
}

// ------------------------------------------------------------ CAPS WORD / LOCK

bool caps_word_press_user(uint16_t keycode) {
    switch (keycode) {
        // Keycodes that continue Caps Word, with shift applied.
        case KC_A ... KC_Z:
        // case KC_MINS:
            add_weak_mods(MOD_BIT(KC_LSFT));  // Apply shift to next key.
            return true;

        // Keycodes that continue Caps Word, without shifting.
        case KC_1 ... KC_0:
        case KC_BSPC:
        case KC_DEL:
        case KC_UNDS:
            return true;

        default:
            return false; // Deactivate Caps Word.
    }
}

// ------------------------------------------------------------ BLINKY LED BITS

enum profile_states_t {
    STATE_NOTHING,
    STATE_LAYER_LOCKED,
    STATE_CAPS_WORD,
};

#define DELAY_SLOW_BLINKING 700
#define DELAY_FAST_BLINKING 200

static bool profile_led = false;
static uint8_t current_profile_state = STATE_NOTHING;

uint32_t led_blinking(uint32_t trigger_time, void *cb_arg) {
    profile_led = !profile_led;
    writePin(D5, !profile_led);
    switch (current_profile_state) {
        case STATE_LAYER_LOCKED:
            return DELAY_SLOW_BLINKING;
        case STATE_CAPS_WORD:
            return DELAY_FAST_BLINKING;
        case STATE_NOTHING:
        default:
            profile_led = false;
            writePin(D5, !profile_led);
            return 0;
    }
}

void layer_lock_set_user(layer_state_t locked_layers) {
    if (locked_layers > 0) {
        current_profile_state = STATE_LAYER_LOCKED;
        defer_exec(10, led_blinking, NULL);
    } else if (locked_layers == 0 && current_profile_state == STATE_LAYER_LOCKED) {
        current_profile_state = STATE_NOTHING;
    }
}

void housekeeping_task_user(void) {
    if (is_caps_word_on() && current_profile_state == STATE_NOTHING) {
        current_profile_state = STATE_CAPS_WORD;
        defer_exec(10, led_blinking, NULL);
    } else if (!is_caps_word_on() && current_profile_state == STATE_CAPS_WORD) {
        current_profile_state = STATE_NOTHING;
    }
}

// ------------------------------------------------------------ DEFAULT INIT

layer_state_t layer_state_set_user(layer_state_t state) {
    uprintf("Layer changed to %u / ", state);
    switch (get_highest_layer(state)) {
        case _QWERTY:
            dprint("querty\n");
            break;
        case _COLEMAK:
            dprint("colemak\n");
            break;
        case _NAVIGATION:
            dprint("navigation\n");
            break;
        case _NUMBERS:
            dprint("numbers\n");
            break;
        case _SYMBOLS:
            dprint("symbols\n");
            break;
        case _FUNCTION:
            dprint("function\n");
            break;
        case _ADJUST:
            dprint("adjust\n");
            break;
    }

    return state;
}

uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case GUI_A:
        case GUI_SC:
        case ALT_S:
        case ALT_L:
        case CALT_R:
        case CALT_I:
            return TAPPING_TERM + 15;
        case SFT_F:
        case SFT_J:
        case CSFT_T:
        case CSFT_N:
            return TAPPING_TERM - 15;
        default:
            return TAPPING_TERM;
    }
}

void keyboard_post_init_user(void) {
    debug_enable   = false;
    debug_matrix   = false;
    debug_keyboard = false;
}
